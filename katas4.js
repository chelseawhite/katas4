const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

let header1 = document.createElement('h3');
header1.textContent = 'Kata 1';
document.body.appendChild(header1);
    function kata1() {
        let gotCitiesArray = gotCitiesCSV.split (",");
        let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(gotCitiesArray);
            document.body.appendChild(newElement)
        return gotCitiesArray;
    }
kata1()

let header2 = document.createElement('h3');
header2.textContent = 'Kata 2';
document.body.appendChild(header2);
    function kata2() {
        let bestThingArray = bestThing.split(" ");
        let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(bestThingArray);
            document.body.appendChild(newElement)
        return bestThingArray;
    }
kata2();

let header3 = document.createElement('h3');
header3.textContent = 'Kata 3';
document.body.appendChild(header3);
    function kata3() {
        let gotCitiesWithSemiColons = kata1().join(';');
        let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(gotCitiesWithSemiColons);
            document.body.appendChild(newElement)
        return gotCitiesWithSemiColons;
    }
kata3();

let header4 = document.createElement('h3');
header4.textContent = 'Kata 4';
document.body.appendChild(header4);
    function kata4() {
        let lotrCitiesCSV = lotrCitiesArray.join(',');
        let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(lotrCitiesCSV);
            document.body.appendChild(newElement)
        return lotrCitiesCSV;
 }
kata4();

let header5 = document.createElement('h3');
header5.textContent = 'Kata 5';
document.body.appendChild(header5);
    function kata5() {
        let lotrFirstFiveCities = lotrCitiesArray.slice(0, 5);
        let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(lotrFirstFiveCities);
            document.body.appendChild(newElement)
        return lotrFirstFiveCities;
    }
kata5();

let header6 = document.createElement('h3');
header6.textContent = 'Kata 6';
document.body.appendChild(header6);
   function kata6() {
       let lotrLastFiveCities = lotrCitiesArray.slice(3);
       let newElement = document.createElement('div');
            newElement.textContent = JSON.stringify(lotrLastFiveCities);
            document.body.appendChild(newElement)
        return lotrLastFiveCities;
   }
kata6();

let header7 = document.createElement('h3');
header7.textContent = 'Kata 7';
document.body.appendChild(header7);
   function kata7() {
       let lotrThirdToFifthCities = lotrCitiesArray.slice(2, 5);
       let newElement = document.createElement('div');
       newElement.textContent = JSON.stringify(lotrThirdToFifthCities);
       document.body.appendChild(newElement)
    return lotrThirdToFifthCities;
   }
kata7();

let header8 = document.createElement('h3');
header8.textContent = 'Kata 8';
document.body.appendChild(header8);
    function kata8() {
        lotrCitiesArray.splice(2, 1);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata8();

let header9 = document.createElement('h3');
header9.textContent = 'Kata 9';
document.body.appendChild(header9);
    function kata9() {
        lotrCitiesArray.splice(5, 2);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata9();

let header10 = document.createElement('h3');
header10.textContent = 'Kata 10';
document.body.appendChild(header10);
    function kata10() {
        lotrCitiesArray.splice(2, 0, 'Rohan');
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata10();

let header11 = document.createElement('h3');
header11.textContent = 'Kata 11';
document.body.appendChild(header11);
    function kata11() {
        lotrCitiesArray.splice(5, 1, 'Deadest Marshes');
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata11();

let header12 = document.createElement('h3');
header12.textContent = 'Kata 12';
document.body.appendChild(header12);
    function kata12() {
        bestThingFirstFourteen = bestThing.slice(0, 14);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThingFirstFourteen);
        document.body.appendChild(newElement);
        return bestThingFirstFourteen;
    }
kata12();

let header13 = document.createElement('h3');
header13.textContent = 'Kata 13';
document.body.appendChild(header13);
    function kata13() {
        bestThingLastTwelve = bestThing.slice(-12);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThingLastTwelve);
        document.body.appendChild(newElement);
        return bestThingLastTwelve
    }
kata13();

let header14 = document.createElement('h3');
header14.textContent = 'Kata 14';
document.body.appendChild(header14);
    function kata14() {
        bestThing23To38 = bestThing.slice(23, 38);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThing23To38);
        document.body.appendChild(newElement);
        return bestThing23To38;
    }
kata14();

let header15 = document.createElement('h3');
header15.textContent = 'Kata 15';
document.body.appendChild(header15);
    function kata15() {
        bestThingLastTwelve = bestThing.substring(69);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThingLastTwelve);
        document.body.appendChild(newElement);
        return bestThingLastTwelve
    }
kata15();

let header16 = document.createElement('h3');
header16.textContent = 'Kata 16';
document.body.appendChild(header16);
    function kata16() {
        bestThing23To38 = bestThing.substring(23, 38);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThing23To38);
        document.body.appendChild(newElement);
        return bestThing23To38
    }
kata16();

let header17 = document.createElement('h3');
header17.textContent = 'Kata 17';
document.body.appendChild(header17);
    function kata17() {
        let wordIndexed = 'only';
        let indexOfOnly = bestThing.indexOf(wordIndexed);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(indexOfOnly);
        document.body.appendChild(newElement);
        return indexOfOnly
    }
kata17();

let header18 = document.createElement('h3');
header18.textContent = 'Kata 18';
document.body.appendChild(header18);
    function kata18() {
        let wordIndexed = 'bit';
        let indexOfBit = bestThing.indexOf(wordIndexed);
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(indexOfBit);
        document.body.appendChild(newElement);
        return indexOfBit
    }
kata18();

let header19 = document.createElement('h3');
header19.textContent = 'Kata 19';
document.body.appendChild(header19);
    function kata19() {
        let searchTermDoubleA = 'aa';
        let searchTermDoubleE = 'ee';
        let searchTermDoubleI = 'ii';
        let searchTermDoubleO = 'oo';
        let searchTermDoubleU = 'uu';
        let gotCitiesArray = gotCitiesCSV.split(',');
        let gotCitiesArrayDoubleVowels = []
        for (let i=0; i<gotCitiesArray.length; i++) {
            if (gotCitiesArray[i].includes(searchTermDoubleA) || gotCitiesArray[i].includes(searchTermDoubleE) || gotCitiesArray[i].includes(searchTermDoubleI) || gotCitiesArray[i].includes(searchTermDoubleO) || gotCitiesArray[i].includes(searchTermDoubleU)) {
                gotCitiesArrayDoubleVowels.push(gotCitiesArray[i])
            }
    }
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(gotCitiesArrayDoubleVowels);
        document.body.appendChild(newElement);
        return gotCitiesArrayDoubleVowels;
    }
kata19();

let header20 = document.createElement('h3');
header20.textContent =  'Kata 20';
document.body.appendChild(header20);
    function kata20() {
        let searchTerm = 'or';
        let lotrCitiesArrayEnding = [];
        for (let i=0; i<lotrCitiesArray.length; i++) {
            if (lotrCitiesArray[i].endsWith(searchTerm)) {
                lotrCitiesArrayEnding.push(lotrCitiesArray[i]);
            }
        }
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArrayEnding);
        document.body.appendChild(newElement);
        return lotrCitiesArrayEnding;
    }
kata20();

let header21 = document.createElement('h3');
header21.textContent = 'Kata 21';
document.body.appendChild(header21);
    function kata21() {
        let searchTerm = 'b';
        let bestThingStart = []
        let bestThingArray = bestThing.split(' ');
        for (let i=0; i<bestThingArray.length; i++) {
            if (bestThingArray[i].startsWith(searchTerm)) {
                bestThingStart.push(bestThingArray[i]);
            }
        }
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(bestThingStart);
        document.body.appendChild(newElement);
        return bestThingStart;
    }

kata21();


let header22 = document.createElement('h3');
header22.textContent = 'Kata 22';
document.body.appendChild(header22);
    function kata22() {
        if (lotrCitiesArray.includes('Mirkwood')) {
            let newElement = document.createElement('div');
            newElement.textContent = 'Yes';
            document.body.appendChild(newElement);
        }
        else {
            let newElement = document.createElement('div');
            newElement.textContent = 'No';
            document.body.appendChild(newElement);
        }
    }
kata22();

let header23 = document.createElement('h3');
header23.textContent = 'Kata 23';
document.body.appendChild(header23);
    function kata23() {
        if (lotrCitiesArray.includes('Hollywood')) {
            let newElement = document.createElement('div');
            newElement.textContent = 'Yes';
            document.body.appendChild(newElement);
        }
        else {
            let newElement = document.createElement('div');
            newElement.textContent = 'No';
            document.body.appendChild(newElement);
        }
    }
kata23();


let header24 = document.createElement('h3');
header24.textContent = 'Kata 24';
document.body.appendChild(header24);
    function kata24() {
        let mirkwoodLocation = lotrCitiesArray.indexOf("Mirkwood");
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(mirkwoodLocation);
        document.body.appendChild(newElement);
        return mirkwoodLocation;
    }
kata24();

let header25 = document.createElement('h3');
header25.textContent = 'Kata 25';
document.body.appendChild(header25);
    function kata25() {
        let cityWithASpace = lotrCitiesArray.find(function(element) {
           return element.includes(' ');
        });
        let newElement = document.createElement('div');
        newElement.textContent = cityWithASpace;
        document.body.appendChild(newElement);
    }
kata25();

let header26 = document.createElement('h3');
header26.textContent = 'Kata 26';
document.body.appendChild(header26);
    function kata26() {
        let reversedLotrArray = lotrCitiesArray;
        reversedLotrArray.reverse();
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(reversedLotrArray);
        document.body.appendChild(newElement);
        return reversedLotrArray;
    }
kata26();

let header27 = document.createElement('h3');
header27.textContent = 'Kata 27';
document.body.appendChild(header27);
    function kata27() {
        let alphaSortedLotrArray = lotrCitiesArray;
        alphaSortedLotrArray.sort();
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(alphaSortedLotrArray);
        document.body.appendChild(newElement);
        return alphaSortedLotrArray;
    }
kata27();


//Found help on this one from SO: https://stackoverflow.com/questions/10630766/how-to-sort-an-array-based-on-the-length-of-each-element
let header28 = document.createElement('h3');
header28.textContent = 'Kata 28';
document.body.appendChild(header28);
    function kata28() {
        let sortedLotrArray = lotrCitiesArray;
        sortedLotrArray.sort(function(a,b) {
            return a.length - b.length;
        });
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(sortedLotrArray);
        document.body.appendChild(newElement);
        return sortedLotrArray;
    }
kata28();

let header29 = document.createElement('h3');
header29.textContent = 'Kata 29';
document.body.appendChild(header29);
    function kata29() {
        lotrCitiesArray.pop();
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata29();

let header30 = document.createElement('h3');
header30.textContent = 'Kata 30';
document.body.appendChild(header30);
    function kata30() {
        lotrCitiesArray.push('Deadest Marshes');
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata30();

let header31 = document.createElement('h3');
header31.textContent = 'Kata 31';
document.body.appendChild(header31);
    function kata31() {
        lotrCitiesArray.shift();
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata31();

let header32 = document.createElement('h3');
header32.textContent = 'Kata 32';
document.body.appendChild(header32);
    function kata32() {
        lotrCitiesArray.unshift('Rohan');
        let newElement = document.createElement('div');
        newElement.textContent = JSON.stringify(lotrCitiesArray);
        document.body.appendChild(newElement);
        return lotrCitiesArray;
    }
kata32();